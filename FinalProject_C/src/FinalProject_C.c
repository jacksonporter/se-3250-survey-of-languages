/*
 ============================================================================
 Name        : FinalProject_C.c
 Author      : Jackson Porter
 Version     :
 Copyright   : Snow College Software Engineering
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <signal.h>
#include <unistd.h>

void startTimer();
double getCurrentTimeDiff();
void startCamera();
void intHandler(int);
void takePicture();

long secondStart;
long milliSecondStart;
double startTime;
struct timespec spec;
CvCapture* capture;

int startedTimer;

int main(void) {
	//Capture SIGINT signal
	signal(SIGINT, intHandler); //Grab a ctrl+c (sigint) kill 
	
	//Set timer started to 0
	startedTimer = 0;
	
	char menuOption;

	printf("**********************Race Timer**********************\n\n");

	//Menu loop
	while(menuOption != 'q'){
		printf("Please choose an option:\n");
		if(startedTimer == 0){ //if timer isn't started, add option to be able to
			printf("(1) Start Timer\n");
		}
		if(startedTimer == 1){ //if timer IS started, add option to log time
			printf("(2) Get Time Difference\n");
		}
		printf("(3) Start Camera\n"); //Print option to start camera viewer
		printf("(q) Quit Program\n"); //Print option to quit 
		printf("\nYour Option: "); //Print line for input
		fflush(stdout); //Flush stdout to allow user input on the same line as last line from stdout
		
		
		scanf("%c", &menuOption); //Get user input
		while(getchar() != '\n'){
			//Throw away rest of the characters typed
		}

		switch(menuOption){
				case '1':
					if(startedTimer == 1){
						printf("Try again, your option wasn't valid.\n");
						printf("The option was: '%c'\n\n", menuOption);
					}
					else{
						startTimer();
						startedTimer = 1;
						printf("\n\nTimer started!\n");
					}
					break;
                case '2':
					if(startedTimer == 0){
						printf("Try again, your option wasn't valid.\n");
						printf("The option was: '%c'\n\n", menuOption);
					}
					else{
						getCurrentTimeDiff();
					}
					break;
                case '3':
					printf("Starting camera!\n");
                    startCamera();
					break;   
                case 'q':
					printf("Closing program.\n");
					break;
				default:
					printf("Try again, your option wasn't valid.\n");
					printf("The option was: '%c'\n\n", menuOption);
					break;
			}
	}
}

void intHandler(int sig){
	printf("\n\nQuitting from CTRL+C\n\n");
	signal(sig, SIG_IGN);
	
	cvReleaseCapture(&capture);
	cvDestroyWindow("Window");

	exit(0);
}

double getTime() {
    struct timespec spec;
	clock_gettime(CLOCK_MONOTONIC, &spec);
    return spec.tv_sec + spec.tv_nsec/1.0e09;
}

void startTimer(){
	//Get starting time of program.
	clock_gettime(CLOCK_MONOTONIC, &spec);
	secondStart = spec.tv_sec;
	milliSecondStart = round(spec.tv_nsec / 1.0e6);
    startTime = getTime();
    
    //Write out start time to a file.
    FILE *f = fopen("times.txt", "w");
    if(f == NULL){
		printf("File : \"times.txt\" could not be opened to write time out.");
		exit(1);
	}
	
	fprintf(f, "**Start System Time: %.3lf seconds**\n\n", startTime);
	
	fclose(f);
}

double getCurrentTimeDiff(){
	/*long seconds;
	long milliseconds;
	struct timespec spec;
	clock_gettime(CLOCK_MONOTONIC, &spec);
	seconds = spec.tv_sec;
	milliseconds = round(spec.tv_nsec / 1.0e6);

	long secs = seconds - secondStart;
	//printf("Difference in seconds: %lu\n", secs);
	long mills = milliseconds - milliSecondStart;
	//printf("Difference in mills seconds: %lu\n", mills);
	if(mills < 0){
		secs = secs - 1;
		mills = 1000 + mills;
	}
	double doublemills = (double) mills;
	//printf("Doublemills: %lf\n", doublemills);

	//convert to double number
	double result = (secs) + (doublemills / 1000);

	//printf("Time passed since start of program: %lu, %lu\n", secs, mills);
	printf("Time returning: %lf seconds\n", result);
	return result;*/
    double elapsed = getTime() - startTime;
    char timeString[10];
    sprintf(timeString, "%f", elapsed);
    takePicture(timeString);
    printf("Time logged: = %.3lf\n", elapsed);
    
    //Write time out to file
    FILE *f = fopen("times.txt", "a");
    if(f == NULL){
		printf("File : \"times.txt\" could not be opened to write time out.");
		exit(1);
	}
	
	fprintf(f, "Time Out: %.3lf seconds\n", elapsed);
	
	fclose(f);
	
	
	
	return elapsed;
}

void takePicture(char message[]){
	char c = 'f';
    
    
    while(c == 'f'){
		capture = cvCaptureFromCAM(CV_CAP_ANY);
		if(!capture){
			printf("Error. Cannot capture. Trying again in 1 second.\n");
			sleep(1);
		}
		else{
			c = 't';
		}
	}
	
	c = 'f';
	IplImage* frame;
	
	while(c == 'f'){
		frame = cvQueryFrame(capture);
		if(!frame){
			printf("Error. Cannot get the frame.\n");
			break;
		}
		else{
			c = 't';
		}
	}
	int length = strlen(message) + 3;
	char path[length];
	for(int i = 0; i < strlen(message) + 1; i++){
		//printf("I'm on %d\n", i);
		if(i < strlen(message)){
				path[i] = message[i];
		}
		else{
			path[strlen(path) - 3] = '.';
			path[strlen(path) - 2] = 'p';
			path[strlen(path) - 1] = 'n';
			path[strlen(path) - 0] = 'g';
		}
	}
	
	//printf("YELL: ");
	//printf(path);
	
	cvSaveImage(path, frame, 0);
	cvReleaseCapture(&capture);
	
}

void startCamera(){    
    char c = 'f';
    
    cvNamedWindow("Window", CV_WINDOW_AUTOSIZE);
    
    while(c == 'f'){
		//capture = cvCreateCameraCapture(0);
		capture = cvCaptureFromCAM(CV_CAP_ANY);
		if(!capture){
			printf("Error. Cannot capture. Trying again in 1 second.\n");
			sleep(1);
		}
		else{
			c = 't';
		}
	}
	while(1){
		IplImage* frame = cvQueryFrame(capture);
		if(!frame){
			printf("Error. Cannot get the frame.\n");
			break;
		}
		cvShowImage("Window", frame);
		cvWaitKey(30);
		//sleep(1);
	}
	cvReleaseCapture(&capture);
	cvDestroyWindow("Window");
}

