﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace HelloWorld
{
    class Program
    {

        List<String> names;
        List<int> scores;

        static void Main(string[] args)
        {
            //Instantiate Global Variables
            names = new List<String>();


            //Console.WriteLine("This program will sort the names within a file, and calculate its point score.");
            //Console.WriteLine("For example,  COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a score of 938 × 53 = 49714.");

            //Console.Write("Please enter the (full) path to the names file: ");
            //var path = Console.ReadLine();

            var fileName = @"/home/jackson/bitbucket/se-3250-survey-of-languages/NameSorter/names.txt";

            String[] fileContent = readAllLines(fileName);
            Console.WriteLine("I read from the file: " + fileName);

            //fileContent = sortLines(fileContent);
            //Console.WriteLine("I sorted the data from the file: " + fileName);

            //Read a line of data out
            //readDataFromLine(fileContent[0]);


            //Put all names from all lines into a list.
            putLinesIntoList(fileContent);

            //Sort list alphabetically. 
            names = names.OrderBy(a => a.Split(','));


            //Calculate names scores
            calculateNameScores();

            //Sum scores
            int total = sumNameScores();



            //Don't close program until user presses a key. 
            Console.Write("\nPress any key to exit...");
            Console.ReadKey();

        }

        public static void sumNameScores()
        {
            int total = 0;

            for(int i = 0; i < scores.Length; i++)
            {
                total += scores[i];    
            }
        }

        
        public static void calculateNameScores()
        {
            for(int i = 0; i < names.Length; i++)
            {
                char[] tempCharArray = names[i].toCharArray();
                int total = 0;

                for(int k = 0; k < tempCharArray.Length; i++)
                {
                    int value = tempCharArray[k];

                    //if we had lowercase letters, we would need to check for them. We are assuming all capitilized names. 


                    //Add to score (ASCII value of A is 65)
                    value = value - 64;
                    total += value;
                }

                scores.add(total * (i+1));
            }
        }


        public static String[] readAllLines(String path)
        {
            String[] lines = File.ReadAllLines(path);
            return lines;
        }

        public static void putLinesIntoList(String[] lines)
        {
            for(int i = 0; i < lines.Length; i++)
            {
                putNamesIntoList(lines[i]);
            }
        }

        private static void putNamesIntoList(String line)
        {
            //Create temporary string array of names
            String[] temp = line.Split(',');
            
            for(int i = 0; i < temp.Length; i++)
            {
                names.Add(temp[i]);
            }
        }

        public static String[] sortLines(String[] lines)
        {
            //need to put all data into an array first
            return lines.OrderBy(a => a.Split(',')[1]).ToArray();
        }

        public static String[] readDataFromLine(String line)
        {
            String[] dataRead = line.Split(',');

            for(int i = 0; i < dataRead.Length; i++)
            {
                Console.WriteLine(i + ": " + dataRead[i]);
            }

            return dataRead;
        }
    }
}
