# SE 3250 - Survey of Languages

![](https://www.snow.edu/pr/brand/images/signature.jpg)

##Survey of Languages
#####Some projects were insipred by the code projects at Project Euler (https://projecteuler.net/)

######Content from the course, including course work and other misc. work. Work in this repository is in various programming languages.

This class gave an introduction to many different languages in order to be familiar with the workings of other programming languages and types. 

Break down of applications/projects:

- C# Project: A networked TicTacToe project that allows for two players to play TicTacToe together. GUI created using XAML (WPF). **Wrote in the C# programming language.**

- FinalProject_C: A basic timing application that takes pictures from a camera connected to your computer (like a webcam) at each lap. Could be used for a Race timing system with more work. **Wrote in the C programming language.**

- GUICalculator: An application with persistent data storage, and two different activities. This is the most advanced app that we have built this semester. This app allows one to calculate basic home mortgages and their payments and saves the data each time you edit it to your local device. **Wrote in the Python programming language.**

- NameSorter: Detection of touches and swipes (gestures) are learned in this exercise. Simply move the tiles on the screen up and down until you have formed the sentence.  **Wrote in the C# programming language.**


###_ANOTHER APPLICATION: RockPaperScissors_
Visit the RockPaperScissors repository to view this project: https://bitbucket.org/ktja/rockpaperscissors

