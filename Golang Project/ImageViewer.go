package main

import (
	"image"
	"image/draw"
	"image/png"
	"os"

	"github.com/google/gxui"
	"github.com/google/gxui/drivers/gl"
	"github.com/google/gxui/themes/dark"
)

func appMain(driver gxui.Driver) {
	file, err := os.Open("PATH TO FILE HERE")
	if err != nil {
		panic(err)
	}
	defer file.Close()
	img, err := png.Decode(file)
	editableImage := image.NewRGBA(img.Bounds())
	draw.Draw(editableImage, editableImage.Bounds(), img, image.Point{0, 0}, draw.Src)

	theme := dark.CreateTheme(driver)
	window := theme.CreateWindow(1024, 1024, "This is the Window")
	imageViewer := theme.CreateImage()
	texture := driver.CreateTexture(editableImage, 1.0)
	imageViewer.SetTexture(texture)
	window.AddChild(imageViewer)

	window.OnClose(driver.Terminate)
}

func main() {
	gl.StartDriver(appMain)
}
