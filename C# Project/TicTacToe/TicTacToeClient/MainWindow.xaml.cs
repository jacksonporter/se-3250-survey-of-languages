﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Windows;
using System.Windows.Controls;

namespace TicTacToeClient
{
    public delegate void UpdateMyPlayerNumDel();
    public delegate void UpdateBoardDel();
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        String serverIP;
        String myplayernum;
        String mysymbol, theirsymbol;
        String buttonSelected;
        String playedLocation;
        bool[] enabled;

        public MainWindow()
        {
            serverIP = "";
            myplayernum = "";
            buttonSelected = "";
            playedLocation = "";
            InitializeComponent();
            Thread th = new Thread(NetworkedGame);
            th.IsBackground = true;
            th.Start();

            enabled = new bool[9];

            for(int i = 0; i < enabled.Length; i++)
            {
                enabled[i] = true;
            }

            disableAllButtons();
        }

        private void changeEnabledStatus()
        {
            Dispatcher.Invoke(() =>
            {
                one.IsEnabled = enabled[0];
                two.IsEnabled = enabled[1];
                three.IsEnabled = enabled[2];
                four.IsEnabled = enabled[3];
                five.IsEnabled = enabled[4];
                six.IsEnabled = enabled[5];
                seven.IsEnabled = enabled[6];
                eight.IsEnabled = enabled[7];
                nine.IsEnabled = enabled[8];
            });
        }

        public void disableAllButtons()
        {
            Dispatcher.Invoke(() =>
            {
                one.IsEnabled = false;
                two.IsEnabled = false;
                three.IsEnabled = false;
                four.IsEnabled = false;
                five.IsEnabled = false;
                six.IsEnabled = false;
                seven.IsEnabled = false;
                eight.IsEnabled = false;
                nine.IsEnabled = false;
            });
        }

        private void SetIP(object sender, RoutedEventArgs e)
        {
            serverIP = IP.Text;
            IPLabel.Visibility = System.Windows.Visibility.Hidden;
            IP.Visibility = System.Windows.Visibility.Hidden;
            ServerButton.Visibility = System.Windows.Visibility.Hidden;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String buttonPressed = ((Button)sender).Name;


            if (buttonPressed.Equals("one"))
            {
                buttonSelected = "1";
                one.Content = mysymbol;
                enabled[0] = false;
            }
            else if (buttonPressed.Equals("two"))
            {
                buttonSelected = "2";
                two.Content = mysymbol;
                enabled[1] = false;
            }
            else if (buttonPressed.Equals("three"))
            {
                buttonSelected = "3";
                three.Content = mysymbol;
                enabled[2] = false;
            }
            else if (buttonPressed.Equals("four"))
            {
                buttonSelected = "4";
                four.Content = mysymbol;
                enabled[3] = false;
            }
            else if (buttonPressed.Equals("five"))
            {
                buttonSelected = "5";
                five.Content = mysymbol;
                enabled[4] = false;
            }
            else if (buttonPressed.Equals("six"))
            {
                buttonSelected = "6";
                six.Content = mysymbol;
                enabled[5] = false;
            }
            else if (buttonPressed.Equals("seven"))
            {
                buttonSelected = "7";
                seven.Content = mysymbol;
                enabled[6] = false;
            }
            else if (buttonPressed.Equals("eight"))
            {
                buttonSelected = "8";
                eight.Content = mysymbol;
                enabled[7] = false;
            }
            else if (buttonPressed.Equals("nine"))
            {
                buttonSelected = "9";
                nine.Content = mysymbol;
                enabled[8] = false;
            }

            changeEnabledStatus();
        }

        public void UpdateMyPlayerNum()
        {
            Dispatcher.Invoke(() =>
            {
                ThisPlayer.Content = myplayernum;
            });

            if (myplayernum.Equals("1"))
            {
                mysymbol = "x";
                theirsymbol = "o";
            }
            else
            {
                mysymbol = "o";
                theirsymbol = "x";
            }
        }

        private void setCurrentPlayer(string playernum)
        {
            Dispatcher.Invoke(() =>
            {
                CurrentPlayer.Content = playernum;
            });
        }

        public void UpdateBoard()
        {
            //while (!gameOver)
            //{
            Dispatcher.Invoke(() =>
            {
                if (!playedLocation.Equals(""))
                {
                    if (playedLocation.Equals("1"))
                    {
                        one.Content = theirsymbol;
                        enabled[0] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("2"))
                    {
                        two.Content = theirsymbol;
                        enabled[1] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("3"))
                    {
                        three.Content = theirsymbol;
                        enabled[2] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("4"))
                    {
                        four.Content = theirsymbol;
                        enabled[3] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("5"))
                    {
                        five.Content = theirsymbol;
                        enabled[4] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("6"))
                    {
                        six.Content = theirsymbol;
                        enabled[5] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("7"))
                    {
                        seven.Content = theirsymbol;
                        enabled[6] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("8"))
                    {
                        eight.Content = theirsymbol;
                        enabled[7] = false;
                        playedLocation = "";
                    }
                    else if (playedLocation.Equals("9"))
                    {
                        nine.Content = theirsymbol;
                        enabled[8] = false;
                        playedLocation = "";
                    }

                    changeEnabledStatus();
                }
            });
            
            //}
        }

        private void NetworkedGame()
        {
            try
            {                
                Console.WriteLine("Client Started.");
                
                while(serverIP == "")
                {
                    //Wait until user puts in an IP.
                }

                TcpClient tcpClient = new TcpClient(serverIP, 5000);
                Stream s = tcpClient.GetStream();
                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;
                StreamReader sr = new StreamReader(s);

                string message = sr.ReadLine();
                //MessageBox.Show(message, "Server Message", MessageBoxButton.OK, MessageBoxImage.Information);

                string[] parsedMessage = message.Split(':');
                while (!parsedMessage[0].Equals("o"))
                {
                    if (parsedMessage[0].Equals("m"))
                    {
                        MessageBox.Show(parsedMessage[1], "TicTacToe - Server", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else if (parsedMessage[0].Equals("n"))
                    {
                        myplayernum = parsedMessage[1];
                        UpdateMyPlayerNum();
                    }
                    else if (parsedMessage[0].Equals("p"))
                    {
                        if (parsedMessage[2].Equals("1"))
                        {
                            playedLocation = "1";
                        }
                        else if (parsedMessage[2].Equals("2"))
                        {
                            playedLocation = "2";
                        }
                        else if (parsedMessage[2].Equals("3"))
                        {
                            playedLocation = "3";
                        }
                        else if (parsedMessage[2].Equals("4"))
                        {
                            playedLocation = "4";
                        }
                        else if (parsedMessage[2].Equals("5"))
                        {
                            playedLocation = "5";
                        }
                        else if (parsedMessage[2].Equals("6"))
                        {
                            playedLocation = "6";
                        }
                        else if (parsedMessage[2].Equals("7"))
                        {
                            playedLocation = "7";
                        }
                        else if (parsedMessage[2].Equals("8"))
                        {
                            playedLocation = "8";
                        }
                        else if (parsedMessage[2].Equals("9"))
                        {
                            playedLocation = "9";
                        }

                        UpdateBoard();
                        changeEnabledStatus();
                    }
                    else if (parsedMessage[0].Equals("r"))
                    {
                        setCurrentPlayer(myplayernum);
                        changeEnabledStatus();

                        while (buttonSelected.Equals("")){
                            //Do nothing. Wait for user input on the UI.
                        }

                        sw.WriteLine("{0}", buttonSelected);
                        if(myplayernum.Equals("1"))
                        {
                            setCurrentPlayer("2");
                        }
                        else
                        {
                            setCurrentPlayer("1");
                        }
                        
                        buttonSelected = "";
                        disableAllButtons();
                    }
                    else if (parsedMessage[0].Equals("e"))
                    {
                        MessageBox.Show("Exception: " + parsedMessage[1], "TicTacToe - Exception", MessageBoxButton.OK, MessageBoxImage.Information);
                        Console.WriteLine("Exception: {0}", parsedMessage[1]);
                        Environment.Exit(1);
                    }

                    try
                    {
                        message = sr.ReadLine();
                        parsedMessage = message.Split(':');
                    }
                    catch (NullReferenceException e)
                    {
                        MessageBox.Show("Server sent no message.", "TicTacToe - Exception", MessageBoxButton.OK, MessageBoxImage.Information);
                        Environment.Exit(1);
                    }


                }

                MessageBox.Show("\nGame is over! The winner was: " + parsedMessage[1] + "\nPress OK to quit...", "TicTacToe - Game Over", MessageBoxButton.OK, MessageBoxImage.Information);
                Console.WriteLine("\nClosing program.");
                sw.Close();
                sr.Close();
                s.Close();
                tcpClient.Close();
                Environment.Exit(0);
            }
            catch (System.IO.IOException e)
            {
                MessageBox.Show("\nConnection was closed by server.\nPress OK to quit...", "TicTacToe - Lost Connection", MessageBoxButton.OK, MessageBoxImage.Information);                
                Environment.Exit(0);
            }
            catch (System.Net.Sockets.SocketException e)
            {
                MessageBox.Show("\nConnection was refused by server.\nPress OK to quit...", "TicTacToe - Refused Connection", MessageBoxButton.OK, MessageBoxImage.Information);
                Environment.Exit(0);
            }
            
        }

        
    } 
}
