﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeConsoleClient
{
    class Program
    {
        static int myplayernum;

        static void Main(string[] args)
        {           
            try
            {
                Console.WriteLine("Client Started.\nPlease enter the server IP:");
                String ip = Console.ReadLine();
                TcpClient tcpClient = new TcpClient(ip, 5000);

                Stream s = tcpClient.GetStream();
                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;
                StreamReader sr = new StreamReader(s);

                string message = sr.ReadLine();

                string[] parsedMessage = message.Split(':');
                while (!parsedMessage[0].Equals("o"))
                {
                    if (parsedMessage[0].Equals("m"))
                    {
                        Console.WriteLine("{0}", parsedMessage[1]);
                    }
                    else if (parsedMessage[0].Equals("n"))
                    {
                        myplayernum = Int32.Parse(parsedMessage[1]);
                    }
                    else if (parsedMessage[0].Equals("p"))
                    {
                        Console.WriteLine("Updating board with played spot");
                        //Update spot played
                    }
                    else if (parsedMessage[0].Equals("r"))
                    {
                        Console.WriteLine("Server requesting me to play!");
                        Console.Write("Enter spot to play: ");
                        String response = Console.ReadLine();
                        sw.WriteLine("{0}", response);
                        //get spot to play!
                    }
                    else if (parsedMessage[0].Equals("e"))
                    {
                        Console.WriteLine("Exception: {0}", parsedMessage[1]);
                        Environment.Exit(1);
                    }

                    try
                    {
                        message = sr.ReadLine();
                        parsedMessage = message.Split(':');
                    }catch(NullReferenceException e)
                    {
                        Console.WriteLine("No message was obtained from server!\n\nPress any key to quit.");
                        Console.ReadKey();
                        Environment.Exit(1);
                    }

                    
                }

                Console.Write("\nGame is over! The winner was: {0}\nPress any key to quit...", parsedMessage[1]);
                Console.ReadKey();
                Console.WriteLine("\nClosing program.");
                sw.Close();
                sr.Close();
                s.Close();
                tcpClient.Close();
            }
            catch (System.IO.IOException e)
            {
                Console.WriteLine("\nConnection was closed by server. Press any key to exit.");
                Console.ReadKey();
                Environment.Exit(0);
            }
            catch(System.Net.Sockets.SocketException e)
            {
                Console.WriteLine("\nConnection was refused by server. Press any key to exit.");
                Console.ReadKey();
                Environment.Exit(0);
            }

            
        }
    }
}
