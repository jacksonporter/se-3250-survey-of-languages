﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToeServer
{
    class TicTacToeModel
    {
        private static int side = 3;
        private int[,] game;
        private int turn; //player 1 is a turn of 1, player 2, a turn of 2

        //TicTacToeModel constructor
        public TicTacToeModel(int turn)
        {
            game = new int[side, side];
            ResetGame();
            this.turn = turn;
        }

        
        //Resets the array and the turn
        public void ResetGame()
        {
            turn = 0;

            for(int i = 0; i < game.GetLength(0); i++)
            {
                for(int k = 0; k < game.GetLength(1); k++)
                {
                    game[i, k] = 0;
                }
            }
        }
        
        public bool PlaySpot(int spot)
        {
            switch (spot)
            {
                case 1:
                    return Play(0, 0);
                case 2:
                    return Play(0, 1);
                case 3:
                    return Play(0, 2);
                case 4:
                    return Play(1, 0);
                case 5:
                    return Play(1, 1);
                case 6:
                    return Play(1, 2);
                case 7:
                    return Play(2, 0);
                case 8:
                    return Play(2, 1);
                case 9:
                    return Play(2, 2);
            }

            return false;
        }

        public bool Play(int row, int column)
        {

            if (game[row, column] == 0 && row >= 0 && column >= 0 && row < side && column < side)
            {
                //Set the spot in the array as the current player's number
                game[row, column] = turn;

                //Change the turn to the next player.
                if(turn == 2)
                {
                    turn = 1;
                }
                else
                {
                    turn = 2;
                }

                return true;                
            }
            else
            {                
                return false;
            }
        }

        public int CurrentTurn()
        {
            return turn;
        }

        //Check to see if a spot is empty on the board
        public bool EmptySpot()
        {
            for (int i = 0; i < game.GetLength(0); i++)
            {
                for (int k = 0; k < game.GetLength(1); k++)
                {
                    if(game[i, k] == 0)
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        //Checks to see if there is a winner on the board.
        public int Winner()
        {
            int result = CheckRows();
            if(result != 0)
            {
                return result;
            }

            result = CheckColumns();
            if (result != 0)
            {
                return result;
            }

            result = CheckDiagonals();
            if (result != 0)
            {
                return result;
            }

            return 0;
        }

        //Checks to see if someone has one on the rows
        private int CheckRows()
        {
            for (int i = 0; i < game.GetLength(0); i++)
            {
                if (game[i, 0] == game[i, 1] && game[i, 1] == game[i, 2] && game[i, 0] != 0)
                {
                    return game[i, 0];
                }
                //List<int> rowcontents = new List<int>();
                //for(int k = 0; k < game.GetLength(1); k++){}
            }

            return 0;
        }

        //Checks to see if someone has one on the columns
        private int CheckColumns()
        {
            for (int i = 0; i < game.GetLength(1); i++)
            {
                if (game[0, i] == game[1, i] && game[1, i] == game[2, i] && game[0, i] != 0)
                {
                    return game[0, i];
                }
                //List<int> rowcontents = new List<int>();
                //for(int k = 0; k < game.GetLength(1); k++){}
            }

            return 0;
        }

        //Checks to see if someone has one on the diagonals
        private int CheckDiagonals()
        {
            if (game[0, 0] == game[1, 1] && game[1, 1] == game[2, 2] && game[0, 0] != 0)
            {
                return game[0, 0];
            }
            if (game[2, 0] == game[1, 1] && game[1, 1] == game[0, 2] && game[2, 0] != 0)
            {
                return game[2, 0];
            }
            else
            {
                return 0;
            }
        }

        public Boolean ContinuePlay()
        {
            if(!EmptySpot() || Winner() != 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
