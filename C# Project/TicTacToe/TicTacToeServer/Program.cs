﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TicTacToeServer
{
    class Program
    {
        public static TcpListener listener;
        public static bool acceptingClients;
        public static Socket soc1, soc2;
        public static NetworkStream s1, s2;
        public static StreamWriter sw1, sw2;
        public static StreamReader sr1, sr2;


        static void Main(string[] args)
        {
            Console.WriteLine("TicTacToe Server starting...\n");
            acceptingClients = true;
            Thread th = new Thread(ListenAndThrowThread);
            th.Start();

            while (true)
            {
                while (acceptingClients)
                {
                    Thread.Sleep(2000);
                    Console.WriteLine("Waiting for two players...");
                }

                try
                {
                    ///////RUN GAME///////
                    Random rand = new Random();
                    int firstTurn = rand.Next(1, 2);
                    Console.WriteLine("The first player is Player {0}!", firstTurn);
                    TicTacToeModel model = new TicTacToeModel(firstTurn); //Make a game model object


                    String play = "";
                    bool result = false;
                    while (model.ContinuePlay())
                    {
                        //Console.ReadKey();
                        //Get the play!
                        if (model.CurrentTurn() == 1)
                        {
                            //sw2.WriteLine("m:Waiting for player 1 to play..."); //Print to other player that we're waiting                        
                            do
                            {
                                sw1.WriteLine("r"); //Ask player for play
                                play = sr1.ReadLine(); //Get the play back from the questioned player
                                result = model.PlaySpot(Int32.Parse(play));
                            } while (!result);
                            sw2.WriteLine("p:x:{0}", play); //Send the play to the other player
                        }
                        else
                        {
                            //sw1.WriteLine("m:Waiting for player 2 to play..."); //Print to other player that we're waiting
                            do
                            {
                                sw2.WriteLine("r"); //Ask player for play                           
                                play = sr2.ReadLine(); //Get the play back from the questioned player
                                result = model.PlaySpot(Int32.Parse(play));
                            } while (!result);
                            sw1.WriteLine("p:o:{0}", play); //Send the play to the other player

                        }
                    }

                    int winner = model.Winner();

                    sw1.WriteLine("o:{0}", winner);
                    sw2.WriteLine("o:{0}", winner);
                    Console.WriteLine("\nWe have a winner! The winner was: {0}!", winner);

                    //Close connections
                    //Player 1
                    sw1.Close();
                    sr1.Close();
                    s1.Close();
                    //Player 2
                    sw2.Close();
                    sr2.Close();
                    s2.Close();

                    Console.Write("\nPress any key to restart...");
                    Console.ReadKey();
                    acceptingClients = true;

                    //Give a dummy connection to the thread so it will restart
                    TcpClient tcpClient = new TcpClient("34.218.52.6", 5000);
                    Stream s = tcpClient.GetStream();
                    StreamReader sr = new StreamReader(s);
                    sr.ReadLine();
                    //sr.Close();
                    //s.Close();
                    //Console.WriteLine("{0}", sr.ReadLine());                    
                }
                catch (System.IO.IOException e)
                {
                    Console.Write("\nA client connection was closed. Shutting down. Press any key to quit.");
                    Console.ReadKey();
                    Environment.Exit(1);
                }
                catch (Exception e)
                {
                    Console.Write("\nException caught! Closing. Press any key to exit...");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
            }
        }


        public static void ListenAndThrowThread()
        {
            listener = new TcpListener(5000);
            listener.Start();

            //Get current IP
            Console.WriteLine("Writing to console the current ip address of this machine. If you have an external IP, you can also use that.");
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    Console.WriteLine("IP ADDR: {0}", ip.ToString());
                }
            }
            Console.WriteLine("\n^^^ Use the appropriate IP for game connectivity ^^^");

            while (true)
            {
                if (acceptingClients)
                {
                    //Connect player 1
                    soc1 = listener.AcceptSocket();
                    s1 = new NetworkStream(soc1);
                    sw1 = new StreamWriter(s1);
                    sw1.AutoFlush = true;
                    sr1 = new StreamReader(s1);
                    Console.WriteLine("Client 1 connected!");
                    sw1.WriteLine("n:1");

                    //Connect player 2
                    soc2 = listener.AcceptSocket();
                    s2 = new NetworkStream(soc2);
                    sw2 = new StreamWriter(s2);
                    sw2.AutoFlush = true;
                    sr2 = new StreamReader(s2);
                    Console.WriteLine("Client 2 connected!");
                    sw2.WriteLine("n:2");

                    Console.WriteLine("\nStarting game!");

                    acceptingClients = false;
                }
                else
                {
                    Socket throwSocket = listener.AcceptSocket();
                    NetworkStream throwStream = new NetworkStream(throwSocket);
                    StreamWriter throwStreamWriter = new StreamWriter(throwStream);
                    throwStreamWriter.AutoFlush = true;
                    throwStreamWriter.WriteLine("m:This game is full. Sorry!");
                    throwStreamWriter.Close();
                    throwStream.Close();
                    throwSocket.Close();
                }
            }
        }
    }
}
